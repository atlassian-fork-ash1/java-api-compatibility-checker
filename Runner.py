import subprocess
from xml.dom.minidom import parse
import xml.dom.minidom
import sys,time,zipfile,glob

def compare(script, command):
	time.sleep(2)
	print "\nStarting API comparison..."
	cmd = "perl "+script+" -o old/*.jar -n new/*.jar "+command
	print "Executing : ", cmd
	return subprocess.Popen(cmd, shell=True).wait()

# Download base artifact from maven repo
def downloadArtifact(groupId, artifactId, version):
	depPlugin = "org.apache.maven.plugins:maven-dependency-plugin:2.1:get"
	repoUrl = "https://maven.atlassian.com"

	print ("\nDownloading artifact "+ groupId + ":" + artifactId + ":" + version + " from " + repoUrl)
	exit_status = subprocess.Popen("mvn " + depPlugin + " -DrepoUrl=" + repoUrl + " -Dartifact=" + 
		groupId + ":" + artifactId + ":" + version ,shell=True).wait()
	status = "Successfully" if(exit_status==0) else "Failed"
	print ("\nDownloading of artifact "+ groupId + ":" + artifactId + ":" + version + " finished "+status)
	return exit_status

# Copying artifacts from local maven repo
def copyArtifact(folder, artifact):
	# Create new directory
	subprocess.Popen("mkdir "+folder, shell=True).wait()
	print "\nCopying "+folder+" artifact :", artifact
	subprocess.Popen("cp "+artifact+" "+folder,shell=True).wait()

# Clean previous artifacts
def cleanup():
	print "Starting cleanup"
	time.sleep(2)
	# Clean existing jars
	subprocess.Popen("rm -f *.jar",shell=True)
	subprocess.Popen("rm -f *.txt",shell=True)
	# Clear existing directory
	subprocess.Popen("rm -rf old",shell=True)
	# Clear existing directory
	subprocess.Popen("rm -rf new",shell=True)
	subprocess.call(["touch", "excludeClasses.txt"])
	subprocess.call(["touch", "includeClasses.txt"])

# Scan a jar archieve and return a list of all classes
def scanJarFile(jar_file):
	print "Scanning Jar : ",jar_file
	zf = zipfile.ZipFile(jar_file, 'r')
	classes = []
	try:
		lst = zf.infolist()
		for zi in lst:
			fn = zi.filename
			if fn.endswith('.class'):
				name = fn.replace ("/",".")
				classes.append(name)
	finally:
		zf.close()
	print "Total classes in jar "+jar_file+" is ", len(classes)
	return classes

# Returns lists of classes belongs to package.
# Add to list if class belongs to a package in packages
def getClassList (classes, packages):
	matching = []
	print "Populating matching class list."

	# I am poor in Python, sorry for school style coding :(
	# Replace the below snippet with proper Python style code
	for c in classes:
		for pkg in packages:
			if(c.startswith(str(pkg).strip())):
				matching.append(c)
	print "Number of matching classes : ", len(matching)
	return matching

# Write classes to file
def populateClassesFile (classes, filename):
	print "Writing %d classes to file : %s" %(len(classes),filename)
	subprocess.call(["rm", "-f", filename])
	subprocess.call(["touch", filename])
	f = open(filename, 'w+')
	try:
		for item in classes:
			f.write("%s\n" % item.replace(".class",""))
	finally:
		f.close()

# Return jar file name under a folder
def getFileName(filepath):
	dir = glob.glob(filepath+"/*.jar")
	for jar in dir:
		return jar

def exclude(packages):
	if (len(str(packages).strip()) == 0):
		return
	print "Packages/classes to skip : ",packages
	skipPackages = packages.split(',')
	allClasses = scanJarFile(getFileName ('old'))
	populateClassesFile (getClassList(allClasses, skipPackages), "excludeClasses.txt")

def include(packages):
	if (len(str(packages).strip()) == 0):
		return
	print "Packages/classes to include : ",packages
	includePackages = packages.split(',')
	allClasses = scanJarFile(getFileName ('old'))
	populateClassesFile (getClassList(allClasses, includePackages), "includeClasses.txt")

script = "japi-compliance-checker.pl"
if(len(sys.argv) == 3):
	print "Usage : python Runner.py <my-config.xml> <script>"
	script = sys.argv[2]

M2_REPO = "/opt/bamboo-agent/.m2/repository/"
#M2_REPO = "/Users/huyle/.m2/repository/"


DOMTree = xml.dom.minidom.parse(sys.argv[1])
root = DOMTree.documentElement
artifacts = root.getElementsByTagName("artifact")
exit_status = 0
for artifact in artifacts:
	cleanup()
	print "\nArtifact: %s" % artifact.getAttribute("name")
	groupId = artifact.getElementsByTagName('groupId')[0].childNodes[0].data
	artifactId = artifact.getElementsByTagName('artifactId')[0].childNodes[0].data
	refVersion = artifact.getElementsByTagName('reference-version')[0].childNodes[0].data
	downloadArtifact(groupId, artifactId, refVersion)

	#newVersion = "4.2.0"
	f = open('version', 'r')
	newVersion = f.read().strip()

	group_path = groupId.replace (".", "/")
	refJar = M2_REPO + group_path + "/" + artifactId + "/" + refVersion +"/" + artifactId + "-" + refVersion +"*.jar"
	newJar = M2_REPO + group_path + "/" + artifactId + "/" + newVersion +"/" + artifactId + "-" + newVersion +"*.jar"

	copyArtifact("old", refJar)
	copyArtifact("new", newJar)

	if (artifact.getAttribute("name").find("-api") > 0):
		print "Artifact is an API, using API rules."
		script = "japi-compliance-checker-api-rules.pl"
	command = ""
	if (artifact.getElementsByTagName('exclude')):
		print "\nFound 'exclude' tag in configuration"
		exclude (artifact.getElementsByTagName('exclude')[0].childNodes[0].data)
		command = "-skip-classes excludeClasses.txt"
	if (artifact.getElementsByTagName('include')):
		print "\nFound 'include' tag in configuration"
		include (artifact.getElementsByTagName('include')[0].childNodes[0].data)
		command = "-classes-list includeClasses.txt"
	exit_status += compare(script, command)

status = "COMPATIBLE" if(exit_status==0) else "INCOMPATIBLE"
print "\n"
print "Compatibility testing finished. Your API is %s \n" %(status)
print "Refer report under artifacts"
print "\n"
exit (exit_status)

